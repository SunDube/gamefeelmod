﻿using UnityEngine;

public class Ball : MonoBehaviour {

    public float speed;
    Vector3 initialPosition;
    public float maxHitAngle;
    public AudioClip[] brickSounds;
    public AudioClip[] wallSounds;
    public AudioClip paddleSound;
    public AudioClip deathSound;

    AudioSource audioSourceComponent;
    Rigidbody rigidbodyComponent;

    

    private bool launched = false;


    private GameObject gameInstantiator;


    void Awake() {
        // get references to components
        rigidbodyComponent = GetComponent<Rigidbody>();
        initialPosition = transform.position;
        audioSourceComponent = GetComponent<AudioSource>();
    }

    void Start() {
        // initially serve the ball directly upward
        //rigidbodyComponent.velocity = new Vector3(0, -1, 0);

        gameInstantiator = GameObject.FindGameObjectWithTag("Spawn");

       
    }

    void Update()
    {
        //Serves the ball using spacebar or mouse click if the ball is not moving and the game has started

        if (launched == false && gameInstantiator.GetComponent<Bricks>().setUpComplete == true)
        {
            if (Input.GetKeyDown("space")||Input.GetMouseButtonDown(1)) {

                rigidbodyComponent.velocity = new Vector3(0, -1, 0);
                launched = true;

            }

        }
    }

    void FixedUpdate() {
        // fix the ball's velocity so that it is constant
        rigidbodyComponent.velocity = rigidbodyComponent.velocity.normalized * speed;
    }

    void Reset() {
        // reset the position of the ball to its initial position
        transform.position = initialPosition;
        // sets the velocity to zero so that it can be served
        rigidbodyComponent.velocity = Vector3.zero;
        //makes in launchable
        launched = false;
    }

    void OnTriggerEnter(Collider other) {

        if (other.tag == "Dead Zone") {
            audioSourceComponent.PlayOneShot(paddleSound);
            PeripheralReaction();
            Reset();
        }
    }

    void OnCollisionExit(Collision collision) {
        GameObject temp = collision.gameObject;

        if (temp.tag == "Brick") {
            audioSourceComponent.PlayOneShot(brickSounds[Random.Range(0, brickSounds.Length)]);
        }

        if (temp.tag == "wall")
        {
            audioSourceComponent.PlayOneShot(wallSounds[Random.Range(0, wallSounds.Length)]);
        }

        if (temp.tag == "Player")
        {
            audioSourceComponent.PlayOneShot(paddleSound);
        }

    }

    void PeripheralReaction() {
        GameObject.FindGameObjectWithTag("Player").GetComponent<Paddle>().DeathShake();
        GameObject[] walls = GameObject.FindGameObjectsWithTag("wall");
        for (int i = 0; i <walls.Length; i++)
        {
            walls[i].GetComponent<WallImpact>().DeathPulse();
        }

    }

}
