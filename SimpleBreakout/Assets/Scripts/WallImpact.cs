﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallImpact : MonoBehaviour
{
    public Color flashColour;

    private bool hit = false;

    private Color originalColour;

    private Color displayColour;

    public float flashSpeed;
    
    private void OnCollisionEnter(Collision collision)
    {
        StartCoroutine(Pulse());
    }

    void Start()
    {
        originalColour = gameObject.GetComponent<Renderer>().material.color;
    }
    void Update()
    {
        if (hit == true)
        {
            displayColour = Color.Lerp(displayColour, originalColour, Time.deltaTime*flashSpeed);
        }
        else {
            displayColour = originalColour;
        }
       

        gameObject.GetComponent<Renderer>().material.color = displayColour;
    }

    void SetBack() {
        hit = false;
    }

    public void DeathPulse() {
        StartCoroutine(Pulse());

    }

    IEnumerator Pulse() {
        hit = true;
        displayColour = flashColour;
        yield return new WaitForSeconds(4);
        hit = false;
    }
}
