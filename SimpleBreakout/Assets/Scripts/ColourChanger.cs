﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColourChanger : MonoBehaviour
{
    public Color[] paddleColours;

    //Makes it start with the first colour
    private int currentColour = 0;

    private Color lerpColour;

    // Start is called before the first frame update
    void Start()
    {
        //Set a colour ti start with
        ChangeColour();
    }

    void Update()
    {
        
    }

    void OnCollisionEnter(Collision collision)
    {
        GameObject tempGO = collision.gameObject;
        //Checks if the ball is the object being collided with
        if (tempGO.tag == "Ball")
        {
            //transfers colour to the ball
            TransferColour(tempGO);
        }
    }

    void OnCollisionExit(Collision collision)
    {
        GameObject tempGO = collision.gameObject;
        if (tempGO.tag == "Ball")
        {
            ChangeColour();

            //Increments to the next colour
            currentColour++;
        }
    }

    void ChangeColour()
    {

        //changes the colour of the paddle
        gameObject.GetComponent<Renderer>().material.color = paddleColours[currentColour];
            //Reset the loop once all the colours have been used
            if (currentColour == paddleColours.Length - 1)
            {
                currentColour = 0;
            }
        

    }

    void TransferColour(GameObject Reciever)
    {

        //Change the colour of the colliding object to the current paddle colour
        Reciever.GetComponent<Renderer>().material.color = gameObject.GetComponent<Renderer>().material.color;

    }
}
