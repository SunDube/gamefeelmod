﻿using UnityEngine;

public class Paddle : MonoBehaviour {

    public float maxSpeed;
    public float forceMultipler;
    public Vector3 shakeRadius;
    public float shakeTime;

    private Vector3 startPosition;
    float horizontalInput;
    Rigidbody rigidbodyComponent;

    private GameObject gameInstantiator;

    void Awake() {
        // get references to components
        startPosition = transform.position;
        rigidbodyComponent = GetComponent<Rigidbody>();
        gameInstantiator = GameObject.FindGameObjectWithTag("Spawn");
    }

    void Update() {
        // record input
        horizontalInput = Input.GetAxisRaw("Horizontal");
    }

    void FixedUpdate() {
        // if paddle has not reached top speed
        if (Mathf.Abs(rigidbodyComponent.velocity.x) < maxSpeed && gameInstantiator.GetComponent<Bricks>().setUpComplete == true) {
            // add force to the paddle if player is pressing left or right
            rigidbodyComponent.AddForce(new Vector3(horizontalInput * forceMultipler, 0, 0));
        }
    }

    public void DeathShake() {
        transform.position = startPosition;
        iTween.ShakePosition(gameObject, shakeRadius, shakeTime);
       

    }
}
