﻿using UnityEngine;
using System.Collections;

public class Brick : MonoBehaviour
{
    //Material to change to 
    public Material fadeColour;
    //The speed at which the brick shrinks
    public float shrinkSpeed;
    //Size to shrink to
    private Vector3 shrinkSize = new Vector3(0, 0, 0);
    //original size
    public Vector3 fullSize;
    private bool destroyed = false;
    private bool spawned = false;
    private bool testBrick;
    

		void OnCollisionExit (Collision collision)
		{
        //commences the destruction of the brick
        destroyed = true;	
        
		}

    void RemoveBrick() {
        Destroy(gameObject);
    }

    void Awake()
    {
        StartCoroutine(GrowBrick());  
    }

    void Update()
    {
        if (destroyed == true)
        {
            //Change to material
            gameObject.GetComponent<Renderer>().material = fadeColour;
            //Shrink down
            transform.localScale = Vector3.Lerp(transform.localScale, shrinkSize, shrinkSpeed);
            //Destroy Game object after shrink is done
            Invoke("RemoveBrick", shrinkSpeed);
        }

        if (spawned == false)
        {
            //Grow Brick
            transform.localScale = Vector3.Lerp(transform.localScale, fullSize, 0.2f);
          
        }
    }

    IEnumerator GrowBrick() {
        fullSize = transform.localScale;
        yield return null;
        //if (testBrick == false)
        //{
            transform.localScale = shrinkSize;
            yield return new WaitForSeconds(0.3f);
            spawned = true;
        //}


    }

   

    public void setSizeCheck(bool tester) {
        testBrick = tester;

    }

    public void setSize() {
        transform.localScale = new Vector3(0.6382f, 0.2454f, 0.9819f);
    }

}
