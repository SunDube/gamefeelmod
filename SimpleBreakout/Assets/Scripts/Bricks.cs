﻿using UnityEngine;
using System.Collections;

public class Bricks : MonoBehaviour {
    public GameObject brickPrefab;
    public float gridWidth;
    public float gridHeight;
    public float columnGap;
    public float rowGap;
    public Color[] colors;
    bool isDead;
    private float brickWidth;
    private float brickHeight;
    private float gridLeft;
    private float gridTop;

    //Kepps the ball and paddle form moving until set up is over
    public bool setUpComplete = false;

   

    //Instantiate bricks one by one
    IEnumerator InstantiateBricks() {
        // Instantitate Bricks 
        for (int y = 0; y < gridHeight; y++)
        {
            for (int x = 0; x < gridWidth; x++)
            {
                GameObject brick = Instantiate(brickPrefab) as GameObject;
                if (x == 0) { brick.GetComponent<Brick>().setSize(); }
                brick.transform.parent = transform;
                // place brick according to grid width/height and column and row gaps
                brick.transform.localPosition = new Vector3(gridLeft + x * brickWidth + x * columnGap, gridTop - y * brickHeight - y * rowGap, 0);
                brick.GetComponent<Renderer>().material.color = colors[y / 2 % (int)gridHeight];
                yield return new WaitForSeconds(0.01f);

            }
            

        }
        setUpComplete = true;
    }

    void Awake() {
        // Calculate Brick prefab width and height 
        GameObject tempBrick = Instantiate(brickPrefab) as GameObject;
        brickHeight = tempBrick.GetComponent<Collider>().bounds.size.y;
        brickWidth = tempBrick.GetComponent<Collider>().bounds.size.x;
        
        Destroy(tempBrick.gameObject);

        // Brick grid layout from top-left

        // Calculate the top left 
        gridLeft = -((gridWidth - 1) / 2f * brickWidth) - ((gridWidth / 2f - .5f) * columnGap);
        gridTop = ((gridHeight - 1) / 2f * brickHeight) + ((gridHeight / 2f - .5f) * rowGap);

        StartCoroutine(InstantiateBricks());
        
       
    }

    

    

}

