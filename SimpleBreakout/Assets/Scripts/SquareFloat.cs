﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SquareFloat : MonoBehaviour
{
    public Vector3 floatPos;
    public Vector3 reverse;
    public float bounceTime;
    private bool floating = true;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(CubeFloat());
    }


    IEnumerator CubeFloat() {
        while (floating == true)
        {
            iTween.MoveBy(this.gameObject, iTween.Hash("amount", floatPos, "time", bounceTime, "easetype", iTween.EaseType.easeInOutSine));
            yield return new WaitForSeconds(bounceTime);
            iTween.MoveBy(this.gameObject, iTween.Hash("amount", reverse, "time", bounceTime, "easetype", iTween.EaseType.easeInOutSine));
            yield return new WaitForSeconds(bounceTime);
        }

    }
}
